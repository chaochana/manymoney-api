<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankReferenceInterestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_reference_interest', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('full_name');
			$table->integer('bank_id');
			$table->decimal('interest_rate',4,4);
			$table->date('effective_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_reference_interest');
	}

}
