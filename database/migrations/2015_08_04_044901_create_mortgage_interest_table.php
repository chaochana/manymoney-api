<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMortgageInterestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mortgage_interest', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('mortgage_plan_id');
			$table->integer('from_tenure');
			$table->integer('to_tenure');
			$table->decimal('interest_rate',4,4);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mortgage_interest');
	}

}
