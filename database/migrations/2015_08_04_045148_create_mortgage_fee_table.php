<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMortgageFeeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mortgage_fee', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('mortgage_plan_id');
			$table->string('name');
			$table->text('description');
			$table->decimal('amount',15,4);
			$table->decimal('percentage',4,4);
			$table->string('percentage_of');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mortgage_fee');
	}

}
