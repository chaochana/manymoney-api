<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMortgagePlanTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mortgage_plan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->integer('bank_id');
			$table->integer('mortgage_interest_id');
			$table->integer('mortgage_feature_id');
			$table->integer('mortgage_fee_id');
			$table->decimal('minimum_loan',20,4);
			$table->decimal('maximum_loan',20,4);
			$table->decimal('maximum_margin',4,4);
			$table->decimal('bank_effective_interest_rate',4,4);
			$table->text('bank_effective_interest_rate_description');
			$table->string('bank_reference_interest');
			$table->text('document');
			$table->text('qualification');
			$table->text('objective');
			$table->text('penalty');
			$table->text('security');
			$table->integer('lock-in_period');
			$table->integer('max_tenure');
			$table->text('max_tenure_description');
			$table->text('reference');
			$table->date('effective_date');
			$table->date('expire_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mortgage_plan');
	}

}
