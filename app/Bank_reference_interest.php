<?php 

namespace ManyMoneyAPI;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;

class Bank_reference_interest extends Model {

    protected $table = 'bank_reference_interest';
    protected $dates = ['deleted_at'];

    public function bank(){
        return $this->belongsTo('ManyMoneyAPI\Bank');
    }

    public function mortgage_plan(){
        return $this->belongsTo('ManyMoneyAPI\Mortgage_plan');
    }

    public static function getLatestBankRefernceInterest($bank_id,$bank_reference_interest){
        return Bank_reference_interest::where('bank_id',$bank_id)->where('name', 'LIKE', $bank_reference_interest)->orderBy('effective_date', 'desc')->first();
    }

}
