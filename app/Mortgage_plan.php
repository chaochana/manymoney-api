<?php

namespace ManyMoneyAPI;

use Response;
use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
use Input;

class Mortgage_plan extends Model {

    protected $table = 'mortgage_plan';
    protected $dates = ['deleted_at'];

    public function bank(){
        return $this->belongsTo('ManyMoneyAPI\Bank');
    }

    public function mortgage_interest(){
        return $this->hasMany('ManyMoneyAPI\Mortgage_interest');
    }

    public function bank_reference_interest(){
        return $this->hasMany('ManyMoneyAPI\Bank_reference_interest','name','bank_reference_interest')->where('bank_id', $this->bank_id);;
    }

    public function scopeOfPrincipal($query, $principal){
        return $query->whereRaw("$principal between minimum_loan and maximum_loan");
    }

    public function scopeOfTenure($query, $tenure){
        return $query->whereRaw("$tenure between minimum_tenure and maximum_tenure");
    }

    public static function getMortgageCompareList($principal, $tenure){

        $mortgage_raw_data_list = Mortgage_plan::ofPrincipal($principal)->ofTenure($tenure)->where('active', 1)->get();

        $mortgage_compare_list = array();

        foreach ($mortgage_raw_data_list as $mortgage_plan) {
            $latest_bank_reference_interest_rate = Bank_reference_interest::getLatestBankRefernceInterest($mortgage_plan->bank_id,$mortgage_plan->bank_reference_interest);

            $mortgage_plan_array = json_decode($mortgage_plan, true);

            $mortgage_plan_array['bank_reference_interest_name'] = $latest_bank_reference_interest_rate->name;
            $mortgage_plan_array['bank_reference_interest_rate'] = $latest_bank_reference_interest_rate->interest_rate;
            $mortgage_plan_array['bank_reference_interest_rate_date'] = $latest_bank_reference_interest_rate->effective_date;

            $calculated_total = Mortgage_plan::getMortgageCalculatedTotal($mortgage_plan->id, $principal, $tenure, $latest_bank_reference_interest_rate->interest_rate);

            $mortgage_plan_array['id_and_name'] = $mortgage_plan->bank_id."##".$mortgage_plan->name."##".$mortgage_plan->subname;

            $mortgage_plan_array['calculated_start_principal']      = $calculated_total['start_principal'];
            $mortgage_plan_array['calculated_tenure']               = $calculated_total['tenure'];
            $mortgage_plan_array['calculated_total_payment']        = $calculated_total['total_payment'];
            $mortgage_plan_array['calculated_total_cut']            = $calculated_total['total_cut'];
            $mortgage_plan_array['calculated_total_interest']       = $calculated_total['total_interest'];
            $mortgage_plan_array['calculated_interest_percentage']  = $calculated_total['interest_percentage'];
            $mortgage_plan_array['calculated_first_payment']        = $calculated_total['first_payment'];
            $mortgage_plan_array['calculated_total_payment_at_3']   = $calculated_total['total_payment_at_3'];
            $mortgage_plan_array['calculated_total_cut_at_3']       = $calculated_total['total_cut_at_3'];
            $mortgage_plan_array['calculated_total_interest_at_3']  = $calculated_total['total_interest_at_3'];
            $mortgage_plan_array['calculated_remaining_principal_at_3'] = $calculated_total['remaining_principal_at_3'];
            $mortgage_plan_array['calculated_average_interest_at_3']= $calculated_total['average_interest_at_3'];
            $mortgage_plan_array['calculated_total_payment_at_5']   = $calculated_total['total_payment_at_5'];
            $mortgage_plan_array['calculated_total_cut_at_5']       = $calculated_total['total_cut_at_5'];
            $mortgage_plan_array['calculated_total_interest_at_5']  = $calculated_total['total_interest_at_5'];
            $mortgage_plan_array['calculated_remaining_principal_at_5'] = $calculated_total['remaining_principal_at_5'];
            $mortgage_plan_array['calculated_average_interest_at_5']= $calculated_total['average_interest_at_5'];
            $mortgage_compare_list[] = $mortgage_plan_array;
        }

        return $mortgage_compare_list;
    }

    public static function getMortgageCalculatedTotal($mortgage_plan_id, $principal, $tenure, $bank_reference_interest_rate){

        $start_principal = $principal;

        $tenure_month = $tenure * 12;

        $total_payment = 0;
        $total_interest = 0;
        $total_cut = 0;
        $totals = array();

        $total_payment_at_3 = 0;
        $total_interest_at_3 = 0;
        $total_cut_at_3 = 0;
        $remaining_principal_at_3 = 0;

        $total_payment_at_5 = 0;
        $total_interest_at_5 = 0;
        $total_cut_at_5 = 0;
        $remaining_principal_at_5 = 0;

        $average_interest = 0;
        $average_interest_at_3 = 0;
        $average_interest_at_5 = 0;

        $payment_array = array();
        $interest_array = array();
          
        for ( $i=0 ; $i < $tenure_month ; $i++ ) {       

            $interest_to_cal = Mortgage_interest::getInterest($mortgage_plan_id, $i+1);

            if ( $interest_to_cal < 0 ) {
                $interest_to_cal = $bank_reference_interest_rate + $interest_to_cal;
            }

            $r = $interest_to_cal / 1200.00;

            if (isset($remaining_principal)){
                $principal = $remaining_principal;
            } 

            $principal_plus_interest = $principal * (1+($interest_to_cal/1200));
          
            if ( $principal > 0.01 ){
                $payment = ($principal*pow((1+($interest_to_cal/1200)),($tenure_month-$i) ))/((1-pow((1+($r)),($tenure_month-$i+1)))/(1-(1+($r)))-pow((1+($r)),($tenure_month-$i)));
            } else {
                $payment = 0;
            }

            $payment_array[] = $payment;
            $interest_array[] = $interest_to_cal;
            $total_payment = $total_payment + $payment;
            $monthly_interest = $principal * $r;
            $total_interest = $total_interest + $monthly_interest;
            $monthly_cut = $payment - $monthly_interest;
            $total_cut = $total_cut + $monthly_cut;
            $remaining_principal = $principal - $monthly_cut;

            if ( $i == 35 ){
                $total_payment_at_3 = $total_payment;
                $total_interest_at_3 = $total_interest;
                $total_cut_at_3 = $total_cut;
                $remaining_principal_at_3 = $remaining_principal;
                $average_interest_at_3 = array_sum($interest_array) / count($interest_array);
                $average_interest_at_3 = number_format($average_interest_at_3,2);
            }

            if ( $i == 59 ){
                $total_payment_at_5 = $total_payment;
                $total_interest_at_5 = $total_interest;
                $total_cut_at_5 = $total_cut;
                $remaining_principal_at_5 = $remaining_principal;
                $average_interest_at_5 = array_sum($interest_array) / count($interest_array);
                $average_interest_at_5 = number_format($average_interest_at_5,2);
            }

        }

        $interest_percentage = ( $total_interest / $start_principal ) * 100;

        $totals = array('start_principal' => $start_principal,
                      'tenure' => $tenure,
                      'total_payment' => $total_payment,
                      'total_cut' => $total_cut,
                      'total_interest' => $total_interest,
                      'interest_percentage' => $interest_percentage,
                      'total_payment_at_3' => $total_payment_at_3,
                      'total_interest_at_3' => $total_interest_at_3,
                      'total_cut_at_3' => $total_cut_at_3,
                      'remaining_principal_at_3' => $remaining_principal_at_3,
                      'average_interest_at_3' => $average_interest_at_3,
                      'total_payment_at_5' => $total_payment_at_5,
                      'total_interest_at_5' => $total_interest_at_5,
                      'total_cut_at_5' => $total_cut_at_5,
                      'remaining_principal_at_5' => $remaining_principal_at_5,
                      'average_interest_at_5' => $average_interest_at_5,
                      'first_payment' => $payment_array[1]);

        return $totals;

    }

}
