<?php 

namespace ManyMoneyAPI;

use Illuminate\Database\Eloquent\Model;
use SoftDeletes;
use DB;

class Mortgage_interest extends Model {

    protected $table = 'mortgage_interest';
    protected $dates = ['deleted_at'];

    public function mortgage_plan(){
        return $this->belongsTo('ManyMoneyAPI\Mortgage_plan');
    }

    public function scopeOfTenure($query, $tenure){
        return $query->whereRaw("$tenure between from_tenure and to_tenure");
    }

    public static function getInterest($mortgage_plan_id,$tenure){
        $result = Mortgage_interest::where('mortgage_plan_id', '=',$mortgage_plan_id)->ofTenure($tenure)->first();

        return $result->interest_rate;
    }

}
