<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

//http://localhost:8888/manymoney-api/public/mortgage/payment?interest=7&principal=1000000&period=1
//http://localhost:8888/manymoney-api/public/mortgage/payment?interestY1=0&interestY2=4&principal=1000000&period=2
//http://localhost:8888/manymoney-api/public/mortgage/payment?interest=7&principal=1000000&period=1&manual_payment=300000
Route::get('/mortgage/payment', 'CalculationController@getMortgagePayment');

Route::get('/mortgage/compare', 'MortgageController@getMortgageCompareList');

Route::get('/mortgage/{id}/interest/{tenure}', 'MortgageController@getMortgageInterest');
Route::get('/mortgage/{id}/interest', 'MortgageController@getMortgageAllInterest');

Route::resource('/mortgage', 'MortgageController');

// Route::get('/mortgage-compare', function () {
//     return 'Hello World';
// });

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// Event::listen('illuminate.query', function($query)
// {
//     var_dump($query);
// });