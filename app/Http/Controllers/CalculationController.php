<?php namespace ManyMoneyAPI\Http\Controllers;

use Response;
use ManyMoneyAPI\Http\Requests;
use ManyMoneyAPI\Http\Controllers\Controller;

use Illuminate\Http\Request;

use ManyMoneyAPI\Bank;
use ManyMoneyAPI\Mortgage_plan;

class CalculationController extends Controller {

	public function getMortgagePayment()
	{
    $query = "";

		//Ensure the parameters are set before go ahead with calculation
    if( ( isset($_GET['interest']) || isset($_GET['interestY1']) ) && isset($_GET['principal']) &&  isset($_GET['period']) )
    {
          
  		//Parameter Setup
      //Also remove ',' from principal string that send from FrontEnd
      $principal = floatval(str_replace(",", "", $_GET['principal']));
      //Copy to 'start_principal' for calculation purpose
      $start_principal = $principal;
      $period = $_GET['period'];
      $interest_year_array = array();
      $manual_payment = isset($_GET['manual_payment']) ? $_GET['manual_payment'] : 0;

  		//Setup Interest for Year 1 - 5, default value is "". If it's zero, assign with minimum number
      $interest_year_array[1] = isset($_GET['interestY1']) ? $_GET['interestY1'] : "";
      $interest_year_array[2] = isset($_GET['interestY2']) ? $_GET['interestY2'] : "";
      $interest_year_array[3] = isset($_GET['interestY3']) ? $_GET['interestY3'] : "";
      $interest_year_array[4] = isset($_GET['interestY4']) ? $_GET['interestY4'] : "";
      $interest_year_array[5] = isset($_GET['interestY5']) ? $_GET['interestY5'] : "";
      
      //Setup default value variable
      $period_month = $period * 12;
      $total_payment = 0;
      $total_interest = 0;
      $total_cut = 0;
      $period_month_array = "";
      $payment_array      = "";
      $interest_array     = "";
      $cut_array          = "";
      $remaining_principal_array = "";
      
      //Start calculate interest month by month    
      for ( $month = 0 ; $month < $period_month ; $month++ )
      {   
          //Break if the remailing pricipal is zero
          if (isset($remaining_principal) && $remaining_principal <= 0.01) break;

          //Find out 'interest_to_cal' for a month in loop
          //If overall interest is set, use it as 'interest_to_cal'.     
          if ( isset($_GET['interest']) ){
            $interest_to_cal = $_GET['interest'];
          }
          else //If overall interest is not set, get it from the interest Year by Year
          {
            //Find out - what year is it for the month?
            $year_count = intval(ceil(($month+1)/12));

            //Start from the year in loop, and check back until get the interest.
            while( $year_count > 0 ){
              //If interest of that year is set, use it.
              if ( isset($interest_year_array[$year_count]) )
              {
                if ( $interest_year_array[$year_count] != "" && $interest_year_array[$year_count] > 0 )
                {
                  $interest_to_cal = $interest_year_array[$year_count];
                  break;
                } 
                else if ( $interest_year_array[$year_count] == 0  )
                {
                  $interest_to_cal = 0;
                  break;
                }
              }
              //If interest of the year is NOT set, check the previous year.
              $year_count--;
            }
          }

          // If the calculation has already begun, remaining_principal is set. Use it as principal to cal
          if (isset($remaining_principal)) $principal = $remaining_principal;

          if ( 0 == $interest_to_cal)
          {
            // r is interest per year
            $r = $interest_to_cal / 1200;

            $calculated_payment = $start_principal / $period_month;

            $principal_plus_interest = $principal;
          }
          else
          {
            // Start Calculate interest - Excel Method
            // r is interest per year
            $r = $interest_to_cal / 1200;

            $principal_plus_interest = $principal * ( 1 + ( $interest_to_cal/1200 ));
            
            // If there is pricipal, calculate payment. If principal is 0, payment is 0.
            if ( $principal > 0 ) $calculated_payment = ($principal*pow((1+($interest_to_cal/1200)),($period_month-$month) ))/((1-pow((1+($r)),($period_month-$month+1)))/(1-(1+($r)))-pow((1+($r)),($period_month-$month)));
            else $calculated_payment = 0;
          }

          // Get total value, sum value
          if ( $manual_payment > $calculated_payment )
          {
            $extra_payment = $manual_payment - $calculated_payment;
            $payment = $manual_payment;
          }         
          else 
          {
            $extra_payment = 0;
            $payment = $calculated_payment;
          }

          if ( $principal_plus_interest <= $payment )
          { 
            $payment = $principal_plus_interest;
            $extra_payment = 0;
          }

          $total_payment = $total_payment + $payment; //Accumulate total payment
          $monthly_interest = $principal * $r;        //Calculate monthly interest
          $total_interest = $total_interest + $monthly_interest; //Accumulate interest

          $monthly_cut = $payment - $monthly_interest;
          $total_cut = $total_cut + $monthly_cut;
          $remaining_principal = $principal - $monthly_cut;
          $remaining_principal = ($remaining_principal < 0.0001) ? 0 : $remaining_principal;

          $payments[] = array('year' => ceil(((($month+1)/12))),
          					  'num' => ($month+1),
          					  'interest' => $interest_to_cal,
          					  'principal' => $principal,
                      'monthly_interest' => $monthly_interest,                      
                      'principal_interest' => $principal_plus_interest,
                      'calculated_payment' => $calculated_payment,
          					  'payment' => $payment*1.00,
                      'extra_payment' => $extra_payment,                      
          					  'monthly_cut' => $monthly_cut,
                      'remaining_principal' => $remaining_principal   
          					  );

          $period_month_array .= ($month+1).",";
          $payment_array .= $payment.",";
          $interest_array .= $monthly_interest.",";
          $cut_array .= $monthly_cut.",";
          $remaining_principal_array .= $remaining_principal.",";
      }

      $totals = array('start_principal' => $start_principal,
                      'period' => $period,
                      'total_payment' => $total_payment,
                      'total_cut' => $total_cut,
                      'total_interest' => $total_interest);

      $period_month_array = rtrim($period_month_array, ",");
      $payment_array = rtrim($payment_array, ",");
      $interest_array = rtrim($interest_array, ",");
      $cut_array = rtrim($cut_array, ",");
      $remaining_principal_array = rtrim($remaining_principal_array, ",");       

      $chart_arrays = array('payment_array' => $payment_array,
                            'interest_array' => $interest_array,
                            'cut_array' => $cut_array,
                            'period_month_array' => $period_month_array,
                            'remaining_principal_array' => $remaining_principal_array);
    }

    return Response::json(array(
        'error' => 'false',
        'monthly_payment' => $payments,
        'total' => $totals,
        'chart_array' => $chart_arrays,
        200,
    ));

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
      //
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
