<?php namespace ManyMoneyAPI\Http\Controllers;

use Response;
use Input;
use ManyMoneyAPI\Http\Requests;
use ManyMoneyAPI\Http\Controllers\Controller;

use Illuminate\Http\Request;

use ManyMoneyAPI\Bank;
use ManyMoneyAPI\Mortgage_plan;
use ManyMoneyAPI\Mortgage_interest;
use ManyMoneyAPI\Bank_reference_interest;

class MortgageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$mortgage_plans = Mortgage_plan::with('Bank', 'Mortgage_interest')->where('active', 1)->get();

		return Response::json(array(
		  'error' => 'false',
		  'mortgage_plans' => $mortgage_plans,
		));
	}

	public static function getMortgageCompareList()
	{
		// Get input from GET parameter, or default one
        $principal = null != Input::get('principal') ? Input::get('principal') : 2000000; 
        $tenure = null != Input::get('tenure') ? Input::get('tenure') : 30;

		$mortgage_compare_list = Mortgage_plan::getMortgageCompareList($principal,$tenure);

		return Response::json(array(
		  'error' => 'false',
		  'mortgage_plans' => $mortgage_compare_list,
		));
	}

	public static function getMortgageInterest($id, $tenure)
	{
        $interest = Mortgage_interest::getInterest($id, $tenure);

		$mortgage_plan = Mortgage_plan::find($id);

        $latest_bank_reference_interest_rate = Bank_reference_interest::getLatestBankRefernceInterest($mortgage_plan->bank_id,$mortgage_plan->bank_reference_interest);

        if ( $interest < 0 ) {
            $interest = $latest_bank_reference_interest_rate->interest_rate + $interest;
        }

		return Response::json(array(
		  'error' => 'false',
		  'interest' => $interest,
		  'bank_reference_interest' => $mortgage_plan->bank_reference_interest,
		  'bank_reference_interest_rate' => $latest_bank_reference_interest_rate->interest_rate,
		  'bank_reference_effective_date' => $latest_bank_reference_interest_rate->effective_date,
		));
	}

	public static function getMortgageAllInterest($id)
	{
		$mortgage_plan = Mortgage_plan::find($id);

        $latest_bank_reference_interest_rate = Bank_reference_interest::getLatestBankRefernceInterest($mortgage_plan->bank_id,$mortgage_plan->bank_reference_interest);

		$interest_array = array();

		for ($i=0; $i < ( $mortgage_plan->maximum_tenure * 12 ) ; $i++) { 
			$interest = Mortgage_interest::getInterest($id, $i+1);
			if ( $interest < 0 ) {
            	$interest = $latest_bank_reference_interest_rate->interest_rate + $interest;
        	}

        	$interest_array[] = array($interest);
		}

		return Response::json(array(
		  'error' => 'false',
		  'interest' => $interest_array,
		  'bank_reference_interest' => $mortgage_plan->bank_reference_interest,
		  'bank_reference_interest_rate' => $latest_bank_reference_interest_rate->interest_rate,
		  'bank_reference_effective_date' => $latest_bank_reference_interest_rate->effective_date,
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$mortgage_plan = Mortgage_plan::with('Bank', 'Mortgage_interest')->find($id);

		return Response::json(array(
		  'error' => 'false',
		  'mortgage_plan' => $mortgage_plan,
		));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
