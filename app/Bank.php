<?php 

namespace ManyMoneyAPI;

use SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model {

	protected $table = 'bank';
    protected $dates = ['deleted_at'];

    public function mortgage_plan(){
        return $this->hasMany('ManyMoneyAPI\Mortgage_plan');
    }

    public function bank_reference_interest(){
        return $this->hasMany('ManyMoneyAPI\Bank_reference_interest');
    }
}
